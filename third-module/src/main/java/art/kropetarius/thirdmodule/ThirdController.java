package art.kropetarius.thirdmodule;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class ThirdController {

    @GetMapping
    public String greeting() {
        return "Hello from third controller";
    }
}
