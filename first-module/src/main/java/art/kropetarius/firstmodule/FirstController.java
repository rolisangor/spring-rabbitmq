package art.kropetarius.firstmodule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
public class FirstController {

    private final RabbitMqSender rabbitMqSender;

    @Value("${app.message}")
    private String message;

    @Autowired
    public FirstController(RabbitMqSender rabbitMqSender) {
        this.rabbitMqSender = rabbitMqSender;
    }

    @GetMapping
    public String greeting(){
        return "Hello from first module" + message;
    }

    @PostMapping("/api/user")
    public String publishUserDetails(@RequestBody User user) {
        rabbitMqSender.send(user);
        return message;
    }


}
